#!/bin/bash
set -ex

KERNEL_OBJ=$(realpath $1)
RAMDISK=$(realpath $2)
OUT=$(realpath $3)

HERE=$(pwd)
source "${HERE}/deviceinfo"

case "$deviceinfo_arch" in
    aarch64*) ARCH="arm64" ;;
    arm*) ARCH="arm" ;;
    x86_64) ARCH="x86_64" ;;
    x86) ARCH="x86" ;;
esac

if [ -d "$HERE/ramdisk-overlay" ]; then
    cp "$RAMDISK" "${RAMDISK}-merged"
    RAMDISK="${RAMDISK}-merged"
    cd "$HERE/ramdisk-overlay"
    find . | cpio -o -H newc | gzip >> "$RAMDISK"
fi

if [ -n "$deviceinfo_bootimg_qcdt" ] && $deviceinfo_bootimg_qcdt; then
    skales-dtbtool -p "$KERNEL_OBJ/scripts/dtc/" -o "$HERE/dt.img" "$KERNEL_OBJ/arch/$ARCH/boot/dts/qcom"
    skales-mkbootimg --kernel "$KERNEL_OBJ/arch/$ARCH/boot/Image.gz" --ramdisk "$RAMDISK" --dt "$HERE/dt.img" --base $deviceinfo_flash_offset_base --ramdisk_base $deviceinfo_flash_offset_ramdisk --pagesize $deviceinfo_flash_pagesize --cmdline "$deviceinfo_kernel_cmdline" --output "$OUT"
else
    mkbootimg --kernel "$KERNEL_OBJ/arch/$ARCH/boot/Image.gz-dtb" --ramdisk "$RAMDISK" --base $deviceinfo_flash_offset_base --kernel_offset $deviceinfo_flash_offset_kernel --ramdisk_offset $deviceinfo_flash_offset_ramdisk --second_offset $deviceinfo_flash_offset_second --tags_offset $deviceinfo_flash_offset_tags --pagesize $deviceinfo_flash_pagesize --os_version $deviceinfo_os_version --os_patch_level $deviceinfo_os_patch_level --cmdline "$deviceinfo_kernel_cmdline" -o "$OUT"
fi
